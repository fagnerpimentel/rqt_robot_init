import os
import rospy
import rospkg

from config import Config

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtGui import QWidget, QGridLayout

from PyQt4.QtCore import * 
from PyQt4.QtGui import * 

from PyQt4 import QtGui

class RobotInit(Plugin):

    _config = Config()

    def __init__(self, context):
        super(RobotInit, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('RobotInit')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which should be in the "resource" folder of this package
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_robot_init'), 'resource', 'RobotInit.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('RobotInitUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface
        context.add_widget(self._widget)

        self.load_tree_widgets()

        self._widget.lineEdit_1.setText(self._config._workspace)
        self._widget.lineEdit_2.setText(self._config._launchpath)
        self._widget.lineEdit_3.setText(self._config._smachpath)

        self._widget.lineEdit_1.setEnabled(False)
        self._widget.lineEdit_2.setEnabled(False)
        self._widget.lineEdit_3.setEnabled(False)

        button_start = self._widget.pushButton_1
        button_start.clicked.connect(self.handle_button__start)

        button_stop = self._widget.pushButton_2
        button_stop.clicked.connect(self.handle_button__stop)

        button_add_command = self._widget.pushButton_4
        button_add_command.clicked.connect(self.handle_button__add_command)

        button_remove_command = self._widget.pushButton_3
        button_remove_command.clicked.connect(self.handle_button__remove_command)

        button_set_workspace = self._widget.pushButton_7
        button_set_workspace.clicked.connect(self.handle_button__set_workspace)

        button_set_launch_path = self._widget.pushButton_6
        button_set_launch_path.clicked.connect(self.handle_button__set_launch_path)

        button_set_smach_path = self._widget.pushButton_5
        button_set_smach_path.clicked.connect(self.handle_button__set_smach_path)

    def handle_button__start(self):

        source = "source " + self._config._workspace + "/devel/setup.bash"
        terminal = "gnome-terminal"

        # file = open(os.path.dirname(os.path.realpath(__file__)) + "/" + self._config._scriptFileName,"w") 
         

        # file.write("#!/bin/bash \n") 
        # file.write("source " + self._config._workspace + "/devel/setup.bash \n")

        for item in self._widget.treeWidget_0.findItems("", Qt.MatchContains | Qt.MatchRecursive):
            if (item.checkState(0)>0):
                # file.write(item.text(0) + " & \n")
                bash = 'bash -c \'' + source + ' & ' + item.text(0) + '; exec /bin/bash -i\''
                terminal += ' --tab -e "' + bash + '"'

        for item in self._widget.treeWidget_1.findItems("", Qt.MatchContains | Qt.MatchRecursive):
            if (item.checkState(0)>0):
                # file.write("roslaunch " + os.path.basename(self._config._launchpath) + " " + item.text(0) + " & \n")
                bash = 'bash -c \'' + source + ' & ' + 'roslaunch ' + os.path.basename(self._config._launchpath) + ' ' + item.text(0) + '; exec /bin/bash -i\''
                terminal += ' --tab -e "' + bash + '"'

        for item in self._widget.treeWidget_2.findItems("", Qt.MatchContains | Qt.MatchRecursive):
            if (item.checkState(0)>0):
                # file.write("rosrun " + os.path.basename(self._config._smachpath) + " " + item.text(0) + " & \n")
                bash = 'bash -c \'' + source + ' & ' + 'rosrun ' + os.path.basename(self._config._smachpath) + ' ' + item.text(0) + '; exec /bin/bash -i\''
                terminal += ' --tab -e "' + bash + '"'

        # file.close()
        print(terminal)
        os.system(terminal)

        # os.system("bash " + os.path.dirname(os.path.realpath(__file__)) + "/" + self._config._scriptFileName)

    def _start_process(self, prog, args):
        self._processes = []
        child = QProcess()
        self._processes.append(child)
        child.start(prog, args)

    def handle_button__stop(self):
        os.system("killall roslaunch");
        os.system("killall rosrun");

    def handle_button__add_command(self):
        text, ok = QInputDialog.getText(self._widget, "Robot Init", 'New command:')
        if ok:
            self._config._commands.append(str(text))
            self.load_tree_widgets()

    def handle_button__remove_command(self):
        for item in self._widget.treeWidget_0.findItems("", Qt.MatchContains | Qt.MatchRecursive):
            if (item.checkState(0)>0):
                self._config._commands.remove(item.text(0))    
        self.load_tree_widgets()

    def handle_button__set_workspace(self):
        filename = QFileDialog.getExistingDirectory(self._widget, 'Set workspace', os.getcwd())
        self._config._workspace = filename
        self._widget.lineEdit_1.setText(filename)
        self._config.save_config()

    def handle_button__set_launch_path(self):
        filename = QFileDialog.getExistingDirectory(self._widget, 'Set launch path', os.getcwd())
        self._config._launchpath = filename
        self._widget.lineEdit_2.setText(filename)
        self.load_tree_widgets()

    def handle_button__set_smach_path(self):
        filename = QFileDialog.getExistingDirectory(self._widget, 'Set smach path', os.getcwd())
        self._config._smachpath = filename
        self._widget.lineEdit_3.setText(filename)
        self.load_tree_widgets()


    def load_tree_widgets(self):

        tree_0 = self._widget.treeWidget_0
        tree_0.clear();
        for command in self._config._commands:
            item = QTreeWidgetItem(tree_0)
            item.setText(0, command)
            item.setCheckState(0, Qt.Unchecked)

        tree_1 = self._widget.treeWidget_1
        tree_1.clear();
        for root, dirs, files in os.walk(self._config._launchpath):
            for file in sorted(files):
                if file.endswith(".launch"):
                    item = QTreeWidgetItem(tree_1)
                    item.setText(0, file)
                    item.setCheckState(0, Qt.Unchecked)

        tree_2 = self._widget.treeWidget_2
        tree_2.clear();
        for root, dirs, files in os.walk(self._config._smachpath):
            for file in sorted(files):
                if file.endswith(".py"):
                    item = QTreeWidgetItem(tree_2)
                    item.setText(0, file)
                    item.setCheckState(0, Qt.Unchecked)

        self._config.save_config()


    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
