import os
import json

class Config:

    _configFileName = "config.json"
    # _scriptFileName = "robot_init.sh"

    _workspace = ""
    _launchpath = ""
    _smachpath = ""
    _commands = []

    def __init__(self):
        self.load_config()   

    def load_config(self):
        data = json.load(open(os.path.dirname(os.path.realpath(__file__)) + "/" + self._configFileName))
        self._workspace = data["workspace"]
        self._launchpath = data["launchpath"]
        self._smachpath = data["smachpath"]
        for command in data["commands"]:
            self._commands.append(command)

    def save_config(self):
        data = {}
        data["workspace"] = self._workspace
        data["launchpath"] = self._launchpath
        data["smachpath"] = self._smachpath
        data["commands"] = self._commands
        with open(os.path.dirname(os.path.realpath(__file__)) + "/" + self._configFileName, 'w') as outfile:
            json.dump(data, outfile)


