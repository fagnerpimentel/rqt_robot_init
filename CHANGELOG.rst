^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rqt_robot_init
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.1 (2018-03-08)
------------------
* Open terminal for each command
* Contributors: Fagner Pimentel

1.0.0 (2018-03-05)
------------------
* Add/remove commands
* Auto find launchs and smachs
* Select commands, launchs and smachs for robot initialization
* Create script for initialization
* Contributors: Fagner Pimentel

